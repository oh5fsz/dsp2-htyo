# DSP II -harjoitustyö
----------------------

## Prosessorikoodi
Kääntäminen:
Asenna viimeisin ARM:n GCC-toolchain:
https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads

Kirjoita `make`

Lähetä .bin -tiedosto build-hakemistosta laudalle esim. ST-Linkillä. (st-util/stm32cubeprogrammer/openocd)

## Python-koodi
Vaatimuksena Python3 ja requirements.txt:ssä olevat paketit, sekä pygobject/pygi.

`virtualenv -p /usr/bin/python3 venv`  
`source venv/bin/activate`  
`python control.py --help`  

## Yksikkötestit
Yksikkötestit vaativat ceedlingin ja voidaan suorittaa ajamalla tässä hakemistossa:
`ceedling`