#ifndef _COMM_H
#define _COMM_H

#ifndef IS_X86
#include "arm_math.h"
#endif

#define DSP_STX 0x7C
#define DSP_CMD_LOG 0x00
#define DSP_CMD_DATA_PACKET 0x02
#define DSP_CMD_SET_NOTCH1_FREQ 0x0B
#define DSP_CMD_SET_NOTCH2_FREQ 0x0C
#define DSP_CMD_SET_NOTCH3_FREQ 0x0D
#define DSP_CMD_SET_NOTCH4_FREQ 0x0E
#define DSP_CMD_SET_NOTCH5_FREQ 0x0F
#define DSP_CMD_SET_NOTCH1_R	0x10
#define DSP_CMD_SET_NOTCH2_R	0x11
#define DSP_CMD_SET_NOTCH3_R	0x12
#define DSP_CMD_SET_NOTCH4_R	0x13
#define DSP_CMD_SET_NOTCH5_R	0x14
#define DSP_CMD_INVALID 0xFF


#define DSP_FRAME_OVERHEAD 3
#define DSP_FRAME_MAX_DATA 128
#define DSP_MAX_PACKET_SIZE DSP_FRAME_OVERHEAD + DSP_FRAME_MAX_DATA

#pragma pack(1)
struct dsp_control_packet
{
    uint8_t start;
    uint8_t cmd;
    uint8_t len;
    uint8_t data[DSP_FRAME_MAX_DATA];
};

typedef union
{
    float32_t value_f;
    uint8_t bytes[4];
} float_u;

typedef enum
{
    DSP_COMM_IDLE,
    DSP_COMM_CMD,
    DSP_COMM_LEN,
    DSP_COMM_DATA
} dsp_comm_state_t;


int dsp_comm_packet_generate(struct dsp_control_packet *p, uint8_t cmd, uint8_t *data, uint8_t len);
uint32_t dsp_comm_packet_serialize(struct dsp_control_packet *p, uint8_t *dst, uint32_t size);
void dsp_comm_packet_putc(struct dsp_control_packet *p, uint8_t b);

#endif /* _COMM_H */
