#include <stdint.h>
#include <string.h>
#include "comm.h"

uint32_t len;
volatile dsp_comm_state_t rx_state;
volatile uint8_t data_pending;
uint8_t _cnt;

uint32_t dsp_comm_packet_serialize(struct dsp_control_packet *p, uint8_t *dst, uint32_t size)
{
    if (dst == NULL || p->len > size) {
        return 0;
    }

    dst[0] = p->start;
    dst[1] = p->cmd;
    dst[2] = (uint8_t)p->len;
    if (p->len > 0) {
        memcpy(&dst[3], &p->data[0], p->len);
    }
    return p->len + DSP_FRAME_OVERHEAD;
}

void dsp_comm_packet_putc(struct dsp_control_packet *p, uint8_t b)
{
    switch (rx_state) {
    case DSP_COMM_IDLE:
        if (b == DSP_STX) {
            rx_state = DSP_COMM_CMD;
            p->start = DSP_STX;
            _cnt = 0;
        }
        break;
    case DSP_COMM_CMD:
        p->cmd = b;
        rx_state = DSP_COMM_LEN;
        break;
    case DSP_COMM_LEN:
        p->len = b;
        rx_state = DSP_COMM_DATA;
        break;
    case DSP_COMM_DATA:
        p->data[_cnt] = b;
        _cnt++;
        if (_cnt >= p->len) {
            data_pending = 1;
            rx_state = DSP_COMM_IDLE;
        }
        break;
    }
}

void dsp_comm_reset(struct dsp_control_packet *rxp, struct dsp_control_packet *txp)
{
    memset(&txp, 0, sizeof(struct dsp_control_packet));
    memset(&rxp, 0, sizeof(struct dsp_control_packet));
}

int dsp_comm_packet_generate(struct dsp_control_packet *p, uint8_t cmd, uint8_t *data, uint8_t len)
{
    if (len > DSP_FRAME_MAX_DATA || p == NULL) {
        return -1;
    }
    memset(p, 0, sizeof(struct dsp_control_packet));
    p->start = DSP_STX;
    p->cmd = cmd;
    p->len = len;
    memcpy(&p->data[0], data, len);
    return 0;
}
