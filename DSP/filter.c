#ifndef IS_X86
#include "arm_math.h"
#endif
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"
#include "filter.h"

float32_t src_buffer[256];
float32_t dst_buffer[256];

static void update_coefficients(struct filter *filt, float32_t *b, float32_t *a)
{
	// Normalize with a1 and save to coefficients table.
	// CMSIS wants a1 and a2 inverted.
	filt->coeffs[0] = b[0] / a[0];
	filt->coeffs[1] = b[1] / a[0];
	filt->coeffs[2] = b[2] / a[0];
	filt->coeffs[3] = -(a[1] / a[0]);
	filt->coeffs[4] = -(a[2] / a[0]);
}

void filter_calculate(struct filter *filt, filt_type_t type, float32_t fc, float32_t r)
{
    memset(filt, 0, sizeof(struct filter));
    filt->r = r;
    filt->freq = fc;
    filt->b[0] = 1;
    filt->b[1] = -2*cos(2*M_PI*(fc/SAMPLE_RATE));
    filt->b[2] = 1;

    filt->a[0] = 1;
    filt->a[1] = -2*FILTER_R*cos(2*M_PI*(fc/SAMPLE_RATE));
    filt->a[2] = r * r;
    update_coefficients(filt, &filt->b[0], &filt->a[0]);
    arm_biquad_cascade_df1_init_f32(&filt->_f, 1, &filt->coeffs[0], &filt->state[0]);
}

void filter_run_u8(struct filter *filt, uint8_t *src, uint8_t *dst, uint32_t len_bytes)
{
    arm_q15_to_float((q15_t*)src, &src_buffer[0], len_bytes/2);
    memcpy(&dst_buffer[0], &src_buffer[0], 256);
    filter_run(filt, &src_buffer[0], &dst_buffer[0], len_bytes/2);
    arm_float_to_q15(&dst_buffer[0], (q15_t*)dst, len_bytes/2);
}

void filter_run(struct filter *filt, float32_t *src, float32_t *dst, uint32_t size)
{
    arm_biquad_cascade_df1_f32(&filt->_f, src, dst, size);
}
