#ifndef _FILTER_H
#define _FILTER_H

#include <stdint.h>

#ifndef IS_X86
#include "arm_math.h"
#endif

#ifdef IS_X86
typedef float float32_t;
#endif

#define SAMPLE_RATE 44100
#define FILTER_R 0.997
#define FILTER_MAX_AMOUNT 8

typedef enum {
    FILT_TYPE_IIR_NOTCH
} filt_type_t;

struct filter
{
    float32_t a[3];
    float32_t b[3];
    float32_t coeffs[6];
    float32_t state[4];
    float32_t freq;
    float32_t r;

    filt_type_t type;
    arm_biquad_casd_df1_inst_f32 _f;
    struct filter *next;
};

void filter_calculate(struct filter *filt, filt_type_t type, float32_t fc, float32_t r);
void filter_run(struct filter *filt, float32_t *src, float32_t *dst, uint32_t size);
void filter_samples_convert_to_float(uint8_t *src, uint32_t len_bytes);
void filter_samples_convert_to_q15(uint8_t *dst, uint32_t len_bytes);
void filter_run_u8(struct filter *filt, uint8_t *src, uint8_t *dst, uint32_t len_bytes);

#endif /* _FILTER_H */
