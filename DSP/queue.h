#ifndef _QUEUE_H
#define _QUEUE_H

#include <stdint.h>

typedef struct
{
    uint8_t data[64];
    uint8_t len;
} data_elem_t;

typedef struct
{
    uint32_t size;
    uint32_t head;
    uint32_t tail;
    uint32_t _max;
    data_elem_t elem[128];
} data_q_t;

int data_q_new(data_q_t *q);
int data_q_add(data_q_t *q, uint8_t *src, uint8_t size);
int data_q_pop(data_q_t *q, uint8_t *dst, uint8_t size);
int data_q_destroy(data_q_t *q);

#endif /* _QUEUE_H */
