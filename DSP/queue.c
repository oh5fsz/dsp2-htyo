#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "queue.h"

int data_q_new(data_q_t *q)
{
    memset(q, 0, sizeof(data_q_t));
    q->_max = 127;
    q->size = 0;
    q->head = 0;
    q->tail = 0;
    return 0;
}

int data_q_add(data_q_t *q, uint8_t *src, uint8_t size)
{
    uint32_t new_tail;

    if (q->size == q->_max || size > 64) {
        return -1;
    }
    new_tail = q->tail + 1;
    if (new_tail > q->_max) {
        new_tail = 0;
    }
    memcpy(&q->elem[q->tail].data[0], src, size);
    q->elem[q->tail].len = size;
    q->tail = new_tail;
    q->size += 1;
    return 0;
}

int data_q_pop(data_q_t *q, uint8_t *dst, uint8_t size)
{
    uint32_t new_head;
    int ret = 0;

    if (q->size == 0 || q->elem[q->head].len > size) {
        return -1;
    }
    new_head = q->head + 1;
    if (new_head > q->_max) {
        new_head = 0;
    }
    memcpy(dst, q->elem[q->head].data, q->elem[q->head].len);
    ret = q->elem[q->head].len;
    q->head = new_head;
    q->size -= 1;
    return ret;
}

int data_q_destroy(data_q_t *q)
{
    return 0;
}
