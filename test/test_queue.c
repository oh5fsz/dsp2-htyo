#include "unity.h"
#include "queue.h"
#include <stdint.h>

#define DATA_Q_LEN 128

data_q_t q;

void setUp(void)
{
    data_q_new(&q);
}

void tearDown(void)
{

}

void test_queue_add(void)
{
    uint8_t buf[5] = {"testi"};
    uint8_t ret[5];
    data_q_add(&q, buf, 5);
    TEST_ASSERT_EQUAL_UINT32(0, q.head);
    TEST_ASSERT_EQUAL_UINT32(1, q.tail);
    TEST_ASSERT_EQUAL_UINT32(1, q.size);
    data_q_pop(&q, ret, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(&buf[0], &ret[0], 5);
}

void test_queue_pop(void)
{
    uint8_t buf[5] = {"testi"};
    uint8_t buf2[6] = {"testi2"};
    uint8_t ret[6];

    data_q_add(&q, buf, 5);
    data_q_add(&q, buf2, 6);
    TEST_ASSERT_EQUAL_UINT32(0, q.head);
    TEST_ASSERT_EQUAL_UINT32(2, q.tail);
    TEST_ASSERT_EQUAL_UINT32(2, q.size);

    int rc = data_q_pop(&q, &ret[0], 6);
    TEST_ASSERT_EQUAL_INT(5, rc);
    TEST_ASSERT_EQUAL_UINT32(1, q.head);
    TEST_ASSERT_EQUAL_UINT32(2, q.tail);
    TEST_ASSERT_EQUAL_UINT32(1, q.size);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(buf, ret, 5);
}

void test_queue_tail_wrap(void)
{
    uint8_t buf[5] = {"testi"};
    uint8_t ret[5];
    int i;

    for (i = 0; i < 129; i++) {
        data_q_add(&q, buf, 5);
    }
    TEST_ASSERT_EQUAL_UINT32(0, q.head);
    TEST_ASSERT_EQUAL_UINT32(127, q.tail);
    TEST_ASSERT_EQUAL_UINT32(127, q.size);
    data_q_pop(&q, ret, 5);
    data_q_pop(&q, ret, 5);
    TEST_ASSERT_EQUAL_UINT32(2, q.head);
    TEST_ASSERT_EQUAL_UINT32(127, q.tail);
    TEST_ASSERT_EQUAL_UINT32(125, q.size);
    data_q_add(&q, buf, 5);
    TEST_ASSERT_EQUAL_UINT32(2, q.head);
    TEST_ASSERT_EQUAL_UINT32(0, q.tail);
    TEST_ASSERT_EQUAL_UINT32(126, q.size);
}

void test_queue_overflow(void)
{
    int rc;
    int i;
    uint8_t buf[5] = {"testi"};

    for (i = 0; i != 129; i++) {
        rc = data_q_add(&q, buf, 5);
    }
    TEST_ASSERT_EQUAL_INT(-1, rc);
}
