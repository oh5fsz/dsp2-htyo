#include <string.h>
#include "unity.h"
#include "comm.h"

extern dsp_comm_state_t rx_state;
struct dsp_control_packet p;

void setUp(void)
{
    memset(&p, 0, sizeof(struct dsp_control_packet));
}

void tearDown(void)
{
}

void test_comm_packet_parsing(void)
{
    int i;

    dsp_comm_packet_putc(&p, 0x01);
    TEST_ASSERT_EQUAL(DSP_COMM_IDLE, rx_state);
    dsp_comm_packet_putc(&p, DSP_STX);
    TEST_ASSERT_EQUAL(DSP_COMM_CMD, rx_state);
    uint8_t head[] = {DSP_CMD_DATA_PACKET, 3};
    uint8_t data[] = {0x01, 0x02, 0x03};
    dsp_comm_packet_putc(&p, head[0]);
    dsp_comm_packet_putc(&p, head[1]);
    dsp_comm_packet_putc(&p, data[0]);
    dsp_comm_packet_putc(&p, data[1]);
    dsp_comm_packet_putc(&p, data[2]);
    TEST_ASSERT_EQUAL(DSP_COMM_IDLE, rx_state);
    TEST_ASSERT_EQUAL(DSP_CMD_DATA_PACKET, p.cmd);
    TEST_ASSERT_EQUAL(3, p.len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(data, p.data, 3);
}
