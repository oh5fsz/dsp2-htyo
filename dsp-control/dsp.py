import enum
import logging
import serial
import struct
import time

from copy import copy
from threading import Thread, Lock, Event
from scipy.io import wavfile
from scipy.fftpack import fft

import numpy as np
import matplotlib
matplotlib.use('GTK3Cairo')
import matplotlib.pyplot as plt

DSP_STX = 0x7C
DSP_CMD_LOG = 0x00
DSP_CMD_DATA_PACKET = 0x02
DSP_CMD_SET_NOTCH1_FREQ = 0x0B
DSP_CMD_SET_NOTCH2_FREQ = 0x0C
DSP_CMD_SET_NOTCH3_FREQ = 0x0D
DSP_CMD_SET_NOTCH4_FREQ = 0x0E
DSP_CMD_SET_NOTCH5_FREQ = 0x0F
DSP_CMD_INVALID = 0xFF


class ParserStates(enum.IntEnum):
    DSP_COMM_IDLE = 0x00
    DSP_COMM_CMD = 0x01
    DSP_COMM_LEN = 0x02
    DSP_COMM_DATA = 0x03
    DSP_COMM_CRC = 0x04


class DSPPacket(object):
    START_BYTE = 0x7c

    def __init__(self, data=None):
        self._state = ParserStates.DSP_COMM_IDLE
        self.log = logging.getLogger()
        self.reset()

        if data:
            self.len = data.get('len', 0)
            self.cmd = data.get('cmd', 0)
            self.data = data.get('data', bytearray([]))

    def reset(self):
        self._cnt = 0
        self.state = ParserStates.DSP_COMM_IDLE
        self.len = 0
        self.cmd = 0
        self.data = bytearray([])
        self.valid = False

    @property
    def state(self):
        return self._state

    @property
    def payload(self):
        return self.data

    @state.setter
    def state(self, state):
        self.log.debug('%s -> %s' % (self._state.name, state.name))
        self._state = state

    def serialize(self):
        data = bytearray([])
        data.append(self.START_BYTE)
        data.append(self.cmd)
        data.append(self.len & 0xFF)
        for b in self.data:
            data.append(b)
        return data

    def finish_rx(self):
        self._cnt = 0
        self.state = ParserStates.DSP_COMM_IDLE
        self.log.debug('Data RX ready')
        self.valid = True

    def put_byte(self, data):
        if self.state == ParserStates.DSP_COMM_IDLE:
            if data == self.START_BYTE:
                self.state = ParserStates.DSP_COMM_CMD
            else:
                raise ValueError('Invalid start byte')
        elif self.state == ParserStates.DSP_COMM_CMD:
            self.cmd = data
            self.state = ParserStates.DSP_COMM_LEN
        elif self.state == ParserStates.DSP_COMM_LEN:
            self.len = data
            self.log.debug('Data length: %d' % self.len)
            if self.len > 0:
                self.state = ParserStates.DSP_COMM_DATA
            else:
                self.finish_rx()
        elif self.state == ParserStates.DSP_COMM_DATA:
            self.data.append(data)
            self._cnt += 1
            if self._cnt == self.len:
                self.finish_rx()


class DSPFile(object):
    def __init__(self, dsp, file):
        self.dsp = dsp
        self.file = file
        self.log = logging.getLogger(self.__class__.__name__)
        self.rx_data = bytearray([])
        self.dsp.set_rx_callback(self.rx_cb)

    def rx_cb(self, data):
        self.rx_data.extend(data)

    def process(self, chunk_size=64):
        sample_rate, samples = wavfile.read(self.file)

        samples = bytearray(samples)
        self.samples = samples
        chunks = [samples[i:i+64] for i in range(0, len(samples), 64)]
        self.log.info('Total length: %d' % len(samples))
        now = time.time()
        self.dsp.enable_rx()
        for chunk in chunks:
            self.dsp.write(DSP_CMD_DATA_PACKET, chunk)
        self.dsp.disable_rx()
        self.log.info('Process took: %f seconds' % (time.time() - now ))
        self.log.info('Received %d bytes' % len(self.rx_data))

    def _plot(self, arr):
        N = len(arr)
        T = 1.0 / 44100.0
        x = np.linspace(0.0, N*T, N)
        yf = fft(arr)
        xf = np.linspace(0.0, 1.0/(2.0*T), N//2)

        plt.plot(xf, 2.0/N * np.abs(yf[0:N//2]))
        plt.grid()

    def plot_original(self):
        arr2 = np.frombuffer(self.samples, dtype='int16')
        self._plot(arr2)

    def plot_filtered(self):
        arr = np.frombuffer(self.rx_data, dtype='int16')
        self._plot(arr)

    def plot_both(self):
        plt.subplot(2, 1, 1)
        self.plot_original()
        plt.ylim(0, 400)
        plt.xlim(0, 15000)
        plt.title('Original signal')
        plt.ylabel('Amplitude')
        plt.xlabel('Frequency [Hz]')

        plt.subplot(2, 1, 2)
        self.plot_filtered()
        plt.ylim(0, 400)
        plt.xlim(0, 15000)
        plt.title('Filtered signal')
        plt.ylabel('Amplitude')
        plt.xlabel('Frequency [Hz]')

        plt.show()


class DSP(object):
    def __init__(self, port):
        self._port = None
        self.port = port
        self.open()
        self.log = logging.getLogger(self.__class__.__name__)
        self.rxp = DSPPacket()
        self.rxe = Event()
        self.buf = bytearray([])
        self._rx_cb = None
        self._rxt_stop = Event()

    def enable_rx(self):
        self._rxt = Thread(target=self.rx_thread)
        self._rxt_stop.clear()
        self._rxt.start()

    def disable_rx(self):
        self._rxt_stop.set()
        self._rxt.join(5)

    def set_rx_callback(self, cb):
        self._rx_cb = cb

    def set_filter_freq(self, filter, freq):
        data = bytearray(struct.pack('<f', freq))
        self.enable_rx()
        if filter == 1:
            self.write(DSP_CMD_SET_NOTCH1_FREQ, data)
        elif filter == 2:
            self.write(DSP_CMD_SET_NOTCH2_FREQ, data)
        elif filter == 3:
            self.write(DSP_CMD_SET_NOTCH3_FREQ, data)
        elif filter == 4:
            self.write(DSP_CMD_SET_NOTCH4_FREQ, data)
        elif filter == 5:
            self.write(DSP_CMD_SET_NOTCH5_FREQ, data)
        self.disable_rx()

    def open(self):
        if not self._port:
            self._port = serial.Serial(self.port, baudrate=115200)
            self._port.reset_input_buffer()
            self._port.reset_output_buffer()

    def rx_thread(self):
        while not self._rxt_stop.is_set():
            waiting = self._port.inWaiting()
            if waiting:
                data = self._port.read(waiting)
                for b in data:
                    self.rxp.put_byte(b)
                    if self.rxp.valid:
                        if self._rx_cb:
                            self._rx_cb(self.rxp.payload)
                        self.rxe.set()
                        self.rxp.reset()

    def close(self):
        if self._port:
            self._port.close()
        self._port = None

    def write(self, cmd, data):
        txp = DSPPacket({
            'cmd': cmd,
            'data': data,
            'len': len(data)
        })
        data = txp.serialize()
        self._port.write(data)
        self._port.flush()
        self.rxe.wait()
        self.rxe.clear()

