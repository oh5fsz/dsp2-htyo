import click
import logging
import sys

from dsp import DSP, DSPFile

logging.basicConfig(level=logging.INFO)

@click.group()
def cli():
    pass

@click.command('send_and_plot')
@click.option('--filename', help='wav tile to send')
@click.option('--port', help='cdc serial port', default='/dev/ttyACM0')
def send_and_plot(filename, port):
    if not filename or not port:
        click.echo('Define filename and port')
        return

    dsp = DSP(port)
    f = DSPFile(dsp, filename)
    f.process()
    f.plot_both()

@click.command('set_freq')
@click.option('--port', help='cdc serial port', default='/dev/ttyACM0')
@click.argument('number', nargs=1)
@click.argument('freq', nargs=1)
def set_freq(port, number, freq):
    dsp = DSP(port)
    dsp.set_filter_freq(int(number), float(freq))


if __name__ == "__main__":
    cli.add_command(send_and_plot)
    cli.add_command(set_freq)
    cli()
